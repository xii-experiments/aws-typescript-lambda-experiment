#
# Provides a `cdk` wrapper script that configures:
#    - :cdk-synth       => runs the `cdk synth` command on the given sources
#    - :cdk-bootstrap   => runs the `cdk bootstrap` command on the given sources
#    - :cdk-deploy      => runs the `cdk deploy` command on the given sources
#    - :cdk-destroy     => runs the `cdk destroy` command on the given sources
#

load("@aspect_bazel_lib//lib:copy_to_bin.bzl", _copy_to_bin = "copy_to_bin")
load("@npm//:aws-cdk/package_json.bzl", _cdk_bin = "bin")

_cdk = _cdk_bin.cdk
_cdk_binary = _cdk_bin.cdk_binary

def cdk(
        srcs,
        name = "cdk",
        resources = [],
        deps = []):
    native.exports_files(["cdk.json"])

    _copy_to_bin(
        name = name + "-config",
        srcs = [":cdk.json"],
    )

    _copy_to_bin(
        name = name + "-resources",
        srcs = resources,
    )

    _cdk(
        name = name + "-synth",
        args = ["synth"],
        outs = ["cdk.out"],
        srcs = srcs + [":" + name + "-config", ":" + name + "-resources"] + deps,
        tags = ["manual"],
        #chdir = "$(RULEDIR)",
    )

    _cdk_binary(
        name = name + "-bootstrap",
        args = ["bootstrap"],
        data = [":" + name + "-config"] + srcs + deps,
        tags = ["manual"],
        chdir = native.package_name(),
    )

    _cdk_binary(
        name = name + "-deploy",
        args = ["deploy"],
        data = [":" + name + "-config"] + srcs + deps,
        tags = ["manual"],
        chdir = native.package_name(),
    )

    _cdk_binary(
        name = name + "-destroy",
        args = ["destroy"],
        data = [":" + name + "-config"] + srcs + deps,
        tags = ["manual"],
        chdir = native.package_name(),
    )
